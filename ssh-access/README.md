# SSH Access

## Description
This project allows SNL administrators to grant SSH user access to specific or all environments, both via direct access or through our IaaS jumpservers.

## Guidelines
- Never push to master directly (it is blocked). Merge request must be reviewed and approved by a peer.
- Pipeline will only run on master branch. Do not run a feature branch on infra.
- Always test new code on a single or small group of dev servers first.

## Usage
There are 3 possible roles to run in this pipeline: 

SSH-ACCESS: This role will grant or revoke users SSH access.
JUMPSERVER-ACCESS: This role will update group membership access on the Bastion jump servers
ROOT-PASSWD: This role will update the root password on the hosts with the value specified in our Vault


### SSH ACCESS 

#### Granting / Revoking a user
1. Edit the [roles/ssh-access/vars/main/users.yml][Users file]
2. Add the user within the user dictionary, making sure to specify all the values required
3. Set the user state to 'present' to grant him access or 'absent' to revoke his access
3. Commit to feature branch and request merge into master
4. Once reviewed and approved, merge into master
5. Run the pipeline, job SSH-ACCESS, tag can be 'grant_access' or 'revoke_access'

### JUMPSERVER ACCESS

#### Add/Modify AD group(s) access to Bastion jump servers
1. Edit the host_vars file for the jumpserver you want to modify
2. Modify the group_membership variable with new value
3. Commit to feature branch and request merge into master
4. Once reviewed and approved, merge into master
5. Run the pipeline, job JUMPSERVER-ACCESS, no tag

### ROOT PASSWD

#### Update a root password
1. Edit the root password in Vault 
5. Run the pipeline, job ROOT-PASSWD, no tag



<!-- Common links -->
[Users file]: roles/ssh-access/vars/main/users.yml
